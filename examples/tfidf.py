import calcuraph
import re
import math

from collections import Counter


def count_rows(state, record):
    return {'doc_count': state['doc_count'] + 1}


def split_words(row):
    words = re.findall(r'\w+', row['text'])

    for word in words:
        yield {
            'doc_id': row['doc_id'],
            'word': word.lower()
        }


def unique_elements(rows):
    yield next(rows)


def calculate_tfidf(rows):
    tfidf = {}
    for row in rows:
        tfidf[row['doc_id']] = row['tf'] * row['idf']

    tfidf_array = sorted([(key, value) for key, value in tfidf.items()],
                         key=lambda x: -x[1])
    yield {
        'term': row['word'],
        'index': tfidf_array[0: 3]
    }


def calculate_tf(rows):
    doc_id = next(rows)['doc_id']
    word_count = Counter()
    for row in rows:
        word_count[row['word']] += 1

    total = sum(word_count.values())

    for word, count in word_count.items():
        yield {
            'doc_id': doc_id,
            'word': word,
            'tf': count / total
        }


def calculate_idf(rows):
    count = 0
    for row in rows:
        count += 1

    idf = math.log10(row['doc_count'] / count)

    yield {
        'word': row['word'],
        'idf': idf
    }


def main():
    input_table = calcuraph.Input('input_table')

    docs_count = calcuraph.Fold(
        input_table,
        table_folder=count_rows,
        initial_state={'doc_count': 0}
    )

    words_splitted = calcuraph.Map(
        input_table,
        row_mapper=split_words
    )

    words_unique = calcuraph.Reduce(
        calcuraph.Sort(
            words_splitted,
            columns=('doc_id', 'word')
        ),
        columns=('doc_id', 'word'),
        table_reducer=unique_elements
    )

    words_idf = calcuraph.Reduce(
        calcuraph.Sort(
            calcuraph.Join(
                words_unique,
                docs_count,
                columns=(),
                join_type='left'
            ),
            columns='word'
        ),
        columns='word',
        table_reducer=calculate_idf
    )

    words_tf = calcuraph.Reduce(
        calcuraph.Sort(
            words_splitted,
            columns=('doc_id', 'word')
        ),
        columns='doc_id',
        table_reducer=calculate_tf
    )

    words_tfidf = calcuraph.Reduce(
        calcuraph.Sort(
            calcuraph.Join(
                words_tf,
                words_idf,
                columns='word',
                join_type='inner'
            ),
            columns='word'
        ),
        columns='word',
        table_reducer=calculate_tfidf
    )

    calcuraph.run(
        words_tfidf,
        input_table={
            'input_table': 'test_files/text_corpus.txt'
        },
        output_file='res/tfidf.txt',
        verbose=True
    )

if __name__ == '__main__':
    main()
