import calcuraph
import re


def split_words(row):
    words = re.findall(r'\w+', row['text'])

    for word in words:
        yield {
            'doc_id': row['doc_id'],
            'word': word.lower()
        }


def calculate_word_count(rows):
    count = 0
    for row in rows:
        count += 1

    yield {
        'word': row['word'],
        'count': count
    }


def main():
    input_table = calcuraph.Input('input_table')

    words_splitted = calcuraph.Map(
        input_table,
        row_mapper=split_words
    )

    words_sorted = calcuraph.Sort(
        words_splitted,
        columns='word'
    )

    words_count = calcuraph.Reduce(
        words_sorted,
        columns='word',
        table_reducer=calculate_word_count
    )

    calcuraph.run(
        words_count,
        input_table={'input_table': 'test_files/text_corpus.txt'},
        output_file='res/word_count.txt',
        verbose=True
    )


if __name__ == '__main__':
    main()
