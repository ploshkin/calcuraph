"""
Module for graph calculation.

Basic Concepts:
    Vertex -- vertex of graph;
    Table -- sequence of rows of the same structure;
    Row -- dictionary, consists of data.
"""

import os
import json

from collections import (
    defaultdict,
    Iterable,
    OrderedDict
)
from itertools import tee
import hashlib


def _read_table_from_file(input_file):
    """Read table from specified input file.

    Parameters
    ----------
    input_file : str
        Path to file containing table.
    """

    data = []
    with open(input_file, 'r') as ifile:
        for line in ifile:
            data.append(defaultdict(None, json.loads(line)))
    return data


def _dump_table_to_file(table, output_file):
    """Dump table to specified output file.

    Parameters
    ----------
    output_file : str
        Path to output file.

    table : list of dictionaries
        Otput table.
    """

    output_dir = os.path.dirname(output_file)
    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    with open(output_file, 'w') as ofile:
        for row in table:
            ofile.write(json.dumps(row, sort_keys=True))
            ofile.write('\n')


class CalcuraphError(Exception):
    """Base class for exceptions in this module."""
    pass


class CalcuraphVertex:
    """Base vertex for this module."""
    pass


class _FunctionCalculationHash:
    """The wrapper over computations, so as not to compute \
one vertex on the same data.
    """

    def __init__(self, function):
        """Initialization a wrapper.

        Parameters
        ----------
        function : callable
            Function to be hashed.
        """

        self._prev_hash = None
        self._calculated = None
        self._function = function

    def __call__(self, *args, **kwargs):
        """Wrapper for function.

        Parameters
        ----------
        hash : hash
            Hash of input parameters.
        """

        if kwargs.get('hash', None) is None:
            self._calculated = self._function(*args, **kwargs)
        else:
            if self._prev_hash is None or self._prev_hash != input_hash:
                self._calculated = self._function(*args, **kwargs)
        self._calculated, result = tee(self._calculated)
        return result


class Input(CalcuraphVertex):
    """An input vertex that loads data from a file.

    This vertex has no previous vertices."""

    def __init__(self, source_name='input'):
        """Initialization of Input.

        Parameters
        ----------
        source_name : str
            Name of input table.
        """

        self._source_name = source_name
        self.run = _FunctionCalculationHash(self._run)

    def _run(self, input_table, verbose, hash=None):
        """Run Input.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        Iterator to input table.
        """

        if verbose:
            print(self)
        if self._source_name in input_table:
            if isinstance(input_table[self._source_name], str):
                return iter(
                    _read_table_from_file(input_table[self._source_name])
                )
            else:
                return iter(input_table[self._source_name])
        else:
            raise CalcuraphError(
                'Invalid input: `{}` not found'.format(self._source_name)
            )

    def __repr__(self):
        return 'Input(source_name={})'.format(self._source_name)


class Map(CalcuraphVertex):
    """Map vertex. This vertex has one input vertex."""

    def __init__(self, input_vertex, row_mapper):
        """Initialization of Map.

        Parameters
        ----------
        input_vertex : CalcuraphVertex
            Input vertex of Calcuraph for Map.

        mapper : callable, iterable
            Mapper.
        """

        self._input_vertex = input_vertex
        self._map_row = row_mapper
        self.run = _FunctionCalculationHash(self._run)

    def _run(self, input_table, verbose, hash=None):
        """Run Map.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        table_mapped : iterator
            Iterator to mapped table.
        """

        def map_table(table):
            for row in table:
                for row_mapped in self._map_row(row):
                    yield row_mapped

        table = self._input_vertex.run(input_table, verbose, hash=hash)

        if verbose:
            print(self)

        table_mapped = map_table(table)
        return table_mapped

    def __repr__(self):
        return 'Map(input_vertex={}, row_mapper={})'.format(
            type(self._input_vertex), self._map_row
        )


class Reduce(CalcuraphVertex):
    """Reduce vertex.

    Operation based on the one previus vertex, reduce function,\
 the specified columns of the table.
    """
    def __init__(self, input_vertex, *, columns, table_reducer):
        """Initialization of Reduce.

        Parameters
        ----------
        input_vertex : CalcuraphVertex
            Input vertex of Calcuraph for Reduce.

        columns : str or list of str
            Columns to reduce, must be unique.

        table_reducer : callable, iterable
            Reducer.
        """

        self._input_vertex = input_vertex
        self._reduce_table = table_reducer
        self._columns = columns
        self.run = _FunctionCalculationHash(self._run)

    def _run(self, input_table, verbose, hash=None):
        """Run Reduce.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        table_reduced : iterator
            Iterator to reduced table.
        """

        def reduce_table(table):
            def compare_function(row):
                if isinstance(self._columns, str):
                    return row[self._columns]
                else:
                    return [row[column] for column in self._columns]

            table, table_copy = tee(table)
            if not sorted(table_copy, key=compare_function):
                raise CalcuraphError(
                    'Array prepared for reduce is not sorted'
                )

            is_first_row = True
            prev_row = None
            table_for_reduce = []

            for row in table:
                if prev_row is not None and\
                        compare_function(prev_row) != compare_function(row):
                    for new_row in self._reduce_table(
                        iter(table_for_reduce)
                    ):
                        yield new_row
                    table_for_reduce = []
                table_for_reduce.append(row)
                is_first_row = False
                prev_row = row

            if len(table_for_reduce):
                for new_row in self._reduce_table(
                    iter(table_for_reduce)
                ):
                    yield new_row

        table = self._input_vertex.run(input_table, verbose, hash=hash)
        if verbose:
            print(self)

        table_reduced = reduce_table(table)
        return table_reduced

    def __repr__(self):
        return 'Reduce(input_vertex={}, columns={}, table_reducer={})'.format(
            type(self._input_vertex), self._columns, self._reduce_table
        )


class Sort(CalcuraphVertex):
    """Sort table by keys or specified function.
    """

    def __init__(self, input_vertex, *, columns=None, key_function=None):
        """Initialization of Sort.

        Parameters
        ----------
        input_vertex : CalcuraphVertex
            Input vertex of Calcuraph for Reduce.

        columns : str or list of str
            Columns to sort, must be unique.

        key_function : callable
            Keys sort by this function.
        """

        self._input_vertex = input_vertex
        self._columns = columns
        self._key_function = key_function

        self.run = _FunctionCalculationHash(self._run)

        if self._columns is None and self._key_function is None:
            raise CalcuraphError('Sort vertex haven\'t compare keys \
and compare function.')
        if self._columns is not None and self._key_function is not None:
            raise CalcuraphError('Sort vertex has compare keys \
and compare function. Please choose one.')

    def _run(self, input_table, verbose, hash=None):
        """Run Sort.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        table_sorted : iterator
            Iterator to sorted table.
        """

        def make_sort_key(element):
            if self._key_function is not None:
                return self._key_function(element)
            else:
                if isinstance(self._columns, str):
                    return element[self._columns]
                else:
                    return [element[field] for field in self._columns]

        def sort_table(table):
            for row in sorted(table, key=make_sort_key):
                yield row

        table = self._input_vertex.run(input_table, verbose, hash=hash)
        if verbose:
            print(self)

        table_sorted = sort_table(table)
        return table_sorted

    def __repr__(self):
        return 'Sort(input_vertex={}, columns={})'.format(
            type(self._input_vertex), self._columns
        )


class Fold(CalcuraphVertex):
    """Fold operation.

    Operation based on the one input vertex, fold function and\
 initial state of new table.
    """
    def __init__(self, input_vertex, *, table_folder, initial_state):
        """Initialization of Fold.

        Parameters
        ----------
        input_vertex : CalcuraphVertex
            Input vertex of Calcuraph for Reduce.

        table_folder : callable
            Function performs fold opeartion, takes table and row.

        initial_state : 
            Initial state for fold.
        """

        self._input_vertex = input_vertex
        self._fold_table = table_folder
        self._initial_state = initial_state
        self.run = _FunctionCalculationHash(self._run)

    def _run(self, input_table, verbose, hash=None):
        """Run Fold.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        table_folded : iterator
            Iterator to folded table.
        """

        def fold_table(table):
            result = self._initial_state
            for row in table:
                result = self._fold_table(result, row)
            yield result

        table = self._input_vertex.run(input_table, verbose, hash=hash)
        if verbose:
            print(self)

        table_folded = fold_table(table)
        return table_folded

    def __repr__(self):
        return "Fold(input_vertex={}, table_folder={})".format(
            type(self._input_vertex), self._fold_table
        )


class Join(CalcuraphVertex):
    """Join operation.

    Operation based on the two previus vertices - left and right,\
 specified keys and join type - inner, left, right.
    More info: https://ru.wikipedia.org/wiki/Join_(SQL)
    """

    def __init__(self, input_left_vertex, input_right_vertex, *, columns, join_type):
        """Initialization of Join.

        Parameters
        ----------
        input_left_table : str or list of dictionaries
            Left input table for join.

        input_right_table : str or list of dictionaries
            Right input table for join.

        columns : str or list of str
            Columns for join.

        join_type : str {'inner', 'left', 'right'}
            Type of join.
        """

        self._input_left_vertex = input_left_vertex
        self._input_right_vertex = input_right_vertex
        self._columns = columns
        self._join_type = join_type

        self.run = _FunctionCalculationHash(self._run)

        if self._join_type not in {'inner', 'left', 'right'}:
            raise CalcuraphError('Not supported type of Join: {}'.format(join_type))

    def _run(self, input_table, verbose, hash=None):
        """Run Join.

        Parameters
        ----------
        input_table : str or list of dictionaries
            Input table or name of file containing input table.

        verbose : Boolean, default=False
            Flag for logging.

        hash : hash
            Hash of input.

        Returns
        -------
        table_folded : iterator
            Iterator to folded table.
        """

        def hash_function(row):
                if isinstance(self._columns, str):
                    return row[self._columns]
                else:
                    return tuple(row[column] for column in self._columns)

        def make_hash(table):
            table_hash = defaultdict(list)
            for row in table:
                table_hash[hash_function(row)].append(row)
            return table_hash

        def join_tables_inner(table_left, table_right_hash):
            for row in table_left:
                row_hash = hash_function(row)
                for row_right in table_right_hash[row_hash]:
                    yield {**row, **row_right}

        def join_tables_outer(table_left, table_right_hash):
            for row in table_left:
                row_hash = hash_function(row)
                if len(table_right_hash[row_hash]) == 0:
                    yield row
                else:
                    for row_right in table_right_hash[row_hash]:
                        yield {**row, **row_right}

        table_left = self._input_left_vertex.run(
            input_table,
            verbose,
            hash=hash
        )
        table_right = self._input_right_vertex.run(
            input_table,
            verbose,
            hash=hash
        )

        if verbose:
            print(self)

        if self._join_type == 'inner':
            table_right_hash = make_hash(table_right)
            return join_tables_inner(table_left, table_right_hash)
        elif self._join_type == 'left':
            table_right_hash = make_hash(table_right)
            return join_tables_outer(table_left, table_right_hash)
        elif self._join_type == 'right':
            table_left_hash = make_hash(table_left)
            return join_tables_outer(table_right, table_left_hash)

    def __repr__(self):
        return 'Join(input_left_vertex={}, input_right_vertex={}, columns={}, join_type={})'.format(
            type(self._input_left_vertex), type(self._input_left_vertex),
            self._columns, self._join_type
        )


def _get_hash(input_table):
    """Calculate hash for input table.

    Parameters
    ----------
    input_table : str or list of dictionaries
            Input table or name of file containing input table.

    Returns
    -------
    table_hash : hash
        Hash of input table
    """

    hashes = OrderedDict()
    for row in input_table:
        if isinstance(input_table[row], str):
            with open(input_table[row], 'r') as ifile:
                for line in ifile:
                    hashes['data'] = hashlib.sha256(
                        line.encode('utf-8')
                    ).hexdigest()

    input_table_dump = json.dumps(input_table, sort_keys=True)

    hash_table_without_files = hashlib.sha256(
        input_table_dump.encode('utf-8')
    ).hexdigest()

    table_hash = hashlib.sha256(
        (hash_table_without_files + ''.join(hashes)).encode('utf-8')
    ).hexdigest()

    return table_hash


def run(input_vertex, input_table, output_file=None, verbose=False):
    """Run computations Calcuraph starts from specified vertex.

    Parameters
    ----------
    input_vertex : CalcuraphVertex
        Calculations starts from this vertex.

    input_table : str or list of dictionaries
        Input table or name of file containing input table.

    output_feed : str, default=None
        Name of file to output.

    verbose : Boolean, default=False
        Flag for logging.

    Returns
    -------
    output_table : list of dictionaries
        Resulting table.
    """

    hash = _get_hash(input_table)
    output_table = input_vertex.run(input_table, verbose, hash=hash)

    if output_file is not None:
        _dump_table_to_file(output_table, output_file)
    else:
        return output_table
